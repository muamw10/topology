#!/bin/bash
TS=$(date +%Y%m%d%H%M)
./gradlew clean jar

cp build/libs/topology*.jar ./topology.jar
tar -cjvf topology-${TS}.tar.bz2 topology.jar asiapacific_static.json europe_static.json northamerica_static.json updateRelayTopology.sh
rm topology.jar
