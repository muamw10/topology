# Topology updater for cardano-node

This is a simple tool for updating your cardano-node topology by choosing the lowest latency peers. It create 3 topology files, one for each region of the world. You can use one of them, some of them, or all of them.

I personally, set up some relays with this tool, and others with either manual lists of peers or use other's tools to get a good topology mix.

## Prerequisites
Java 14

## Usage
1.) Download the latest version from the Downloads area.
2.) Extract the bz2 file to a location of your choosing
```
tar -xzvf topology-<timestamp>.tar.bz2
```

3.) Edit `northamerica_static.json`, `europe_static.json`, and `asiapacific_static.json` to include your own relays/cores in the topology or any other's you'd like to always connect to.

4.) Run `./updateRelayTopology.sh` from the relay server to find the lowest-latency peers to that server.

5.) (Optional) edit `updateRelayTopology.sh` to include deploying the new topology file(s) and restarting servers.

6.) (Optional) Automate this process with a cron job.
