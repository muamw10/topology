package network


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import network.Producer

@JsonClass(generateAdapter = true)
data class Topology(
    @Json(name = "Producers")
    val producers: List<Producer> = listOf()
)