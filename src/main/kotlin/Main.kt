import com.squareup.moshi.Moshi
import network.Producer
import network.Topology
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.buffer
import okio.sink
import okio.source
import java.io.File
import java.net.InetSocketAddress
import java.net.Socket
import java.util.*
import kotlin.system.measureTimeMillis

private val northAmerica = TreeSet<Producer> { producer1, producer2 -> producer1.latency.compareTo(producer2.latency) }
private val europe = TreeSet<Producer> { producer1, producer2 -> producer1.latency.compareTo(producer2.latency) }
private val asiaPacific = TreeSet<Producer> { producer1, producer2 -> producer1.latency.compareTo(producer2.latency) }

fun main(args: Array<String>) {
    if (args.size != 2) {
        println(
            """Usage:
            |  ./topology.jar <addr_to_ignore> <number_of_peers>
            """.trimMargin()
        )
        return
    }
    val client = OkHttpClient.Builder().build()
    val response = client.newCall(
        Request.Builder()
            .get()
            .url("https://explorer.cardano.org/relays/topology.json")
            .cacheControl(CacheControl.FORCE_NETWORK)
            .build()
    ).execute()

    val ignoreAddressContaining = args[0]
    val numberOfPeers = args[1].toInt()

    response.body?.source()?.let { source ->
        val adapter = Moshi.Builder().build().adapter(Topology::class.java).indent("  ")
        adapter.fromJson(source)?.let { topology ->
            topology.producers.filter { !it.addr.contains(ignoreAddressContaining) }.shuffled().forEach { producer ->
                try {
                    Socket().use { socket ->
                        val durationMs = measureTimeMillis {
                            socket.connect(InetSocketAddress(producer.addr, producer.port), 1000)
                        }
                        println("${producer.continent}, ${producer.state} - ${producer.addr}:${producer.port}: ${durationMs}ms")
                        producer.latency = durationMs
                        when (producer.continent) {
                            "North America" -> northAmerica.add(producer)
                            "Europe" -> europe.add(producer)
                            else -> asiaPacific.add(producer)
                        }
                    }
                } catch (e: Throwable) {
                    println("${producer.continent}, ${producer.state} - ${producer.addr}:${producer.port}: FAILURE!")
                }
            }
        }

        val northamericaStatic = File("northamerica_static.json").source().buffer().use { northamericaSource ->
            adapter.fromJson(northamericaSource)!!
        }
        val europeStatic = File("europe_static.json").source().buffer().use { europeSource ->
            adapter.fromJson(europeSource)!!
        }
        val asiapacificStatic = File("asiapacific_static.json").source().buffer().use { asiapacificSource ->
            adapter.fromJson(asiapacificSource)!!
        }

        File("northamerica.json").sink().buffer().use { northAmericaSink ->
            adapter.toJson(northAmericaSink, Topology(northamericaStatic.producers + northAmerica.take(numberOfPeers)))
        }

        File("europe.json").sink().buffer().use { europeSink ->
            adapter.toJson(europeSink, Topology(europeStatic.producers + europe.take(numberOfPeers)))
        }

        File("asiapacific.json").sink().buffer().use { asiaPacificSink ->
            adapter.toJson(asiaPacificSink, Topology(asiapacificStatic.producers + asiaPacific.take(numberOfPeers)))
        }
    }
}