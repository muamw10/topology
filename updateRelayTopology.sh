#!/bin/bash
# Ignore pinging any addresses containing this value
IGNORE_ADDR=bluecheese
# Number of lowest latency peers to add to your topology files
NUMBER_OF_PEERS=11

# INPUTS
# northamerica_static.json - topology file containing iohk northamerica servers and your own relays/cores.
# europe_static.json - topology file containing iohk europe servers and your own relays/cores.
# asiapacific_static.json - topology file containing iohk asia-pacific servers and your own relays/cores.

/usr/bin/java -jar topology.jar ${IGNORE_ADDR} ${NUMBER_OF_PEERS}

# OUTPUTS
# lowest latency northamerica.json
# lowest latency europe.json
# lowest latency asiapacific.json

# TODO
# Add commands below to upload new topology files to your relays and restart them.
# 
#
#
